// --- 1 ---

// setTimeout дозволяє викликати функцію один раз через певний проміжок часу. setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний інтервал часу

// --- 2 ---

/// Ні, це не буде міттєво, оскільки планувальник викликатиме функцію лише після завершення виконання поточного коду, а це займе певний час


/// --- 3---

// Функція посилається на зовнішнє лексичне середовище, тому, поки вона живе, зовнішні змінні також живуть. 
// Вони можуть зайняти набагато більше пам’яті, ніж сама функція. Тому, коли нам більше не потрібна запланована функція, краще її скасувати


let slideIndex = 0;
let playing = true;



function carousel() {
  let i;
  const slides = document.querySelectorAll(".image-to-show");
  for (let i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;

  if (slideIndex > slides.length) {
    slideIndex = 1;
  }

  slides[slideIndex - 1].style.display = "block";
}
function pauseSlideshow() {
  playing = false;
  clearInterval(slideInterval);
}
function playSlideshow() {
  playing = true;
  slideInterval = setInterval(carousel, 3000);
}

const wrapper = document.querySelector('.btn-wrapper')

wrapper.addEventListener("click", (ev) => {
  if ((ev.target.className = "btn-stop" && playing)) {
    pauseSlideshow();
  } else if ((ev.target.className = "bnt-continue")) {
    playSlideshow();
  }
});

carousel();
playSlideshow();
